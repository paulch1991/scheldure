package com.example.pavlo.scheldure;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Pasha on 24.11.2015.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private String[] dataBase;
    private int[] image = {R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher};
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView mTextView;
        private ImageView mImage;

        public ViewHolder(View view) {
            super(view);

            mTextView = (TextView) view.findViewById(R.id.text_view);
            mImage = (ImageView) view.findViewById(R.id.image_item);
            // remind this moment
        }
    }
    public MyAdapter(String[] dataBase){
        this.dataBase = dataBase;
    }


    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_text_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        holder.mTextView.setText(dataBase[position]);
        holder.mImage.setImageResource(image[position]);

    }

    @Override
    public int getItemCount() {
        return dataBase.length;
    }
}
