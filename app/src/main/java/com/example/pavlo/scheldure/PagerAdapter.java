package com.example.pavlo.scheldure;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Pasha on 24.11.2015.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int numberOfTabs;
    private OneFragment mOneFragment;
    private TwoFragment mTwoFragment;


    public PagerAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
        mOneFragment = new OneFragment();
        mTwoFragment = new TwoFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0 : return mOneFragment;
            case 1 : return mTwoFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
